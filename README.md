# Heroes 3 picker

This project is intended for generating heroes
for `Random Hero Trade` format for Heroes of Might and Magic 3:
Horn of the Abyss.

The user specifies number of heroes for randomizer.
The randomizer starts with generation of towns, and then
randomly chooses 1 hero for each chosen town.
It is possible to exclude towns and/or heroes from
randomized choice.

There are prebuilt lists
for [JO 2.82](https://www.h3templates.com/templates/jebus-outcast)
and [mt_outcast](https://sites.google.com/view/homm3milord/templates/mt-outcast).
Addition of everything else is welcome as well.

All images and heroes information were
scraped from [heroes.thelazy.net](https://heroes.thelazy.net/index.php/List_of_heroes_(HotA)),
thanks them for organising everything in easy-to-parse way.

## Main function description
Generator function itself is pretty simple, for more
details refer to the code
directly: [h3pick.js](./scripts/h3pick.js#L49)

