const jo_ua_heroes = {
    "allowed": [
        "Thorgrim",
        "Adrienne",
        "Catherine", "Roland",
        "Mutare Drake",
        "Boragus",
        "Xeron"
    ],
    "banned": [
        "Valeska", "Sanya", "Edric", "Christian", "Rion", "Cuthbert", "Ingham",
        "Mephala", "Kyrre", "Coronius", "Malcom", "Clancy", "Uland", "Gem", "Melodia", "Alagar",
        "Thane", "Neela", "Torosar", "Fafner", "Iona", "Serena", "Aine", "Theodorus",
        "Rashka", "Octavia", "Calh", "Xyron", "Ash", "Pyre", "Axsis", "Olema", "Calid", "Xarfax",
        "Septienna", "Nimbus", "Thant", "Moandor", "Charna", "Ranloo", "Xsi",
        "Gunnar", "Synca", "Shakti", "Alamar", "Jeddite", "Geon", "Arlach", "Jaegar", "Darkstorn",
        "Gretchin", "Crag Hack", "Dessa", "Terek", "Gundula", "Yog", "Gird", "Vey", "Zubin",
        "Oris", "Drakon", "Tazar", "Tiva", "Brissa", "Korbac", "Gerwulf", "Rosic", "Verdish", "Merist",
        "Grindan", "Pasis", "Thunar", "Ignissa", "Lacus", "Kalt",
        "Corkes", "Cassiopeia", "Miriam", "Leena", "Dargem",
        "Beatrice", "Giselle",
        "Sylvia", "Voy", "Elmore"
    ]
};
const jo_ua_banned_heroes = get_standard_banned_heroes_list(jo_ua_heroes);
