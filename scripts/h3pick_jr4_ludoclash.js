const jr4_ludoclash_heroes = {
    "allowed": [
        "Catherine", "Roland", "Sir Mullich",  // Castle
        "Gelu", "Thorgrim", // Rampart
        "Dracon",  // Tower
        "Xeron",  // Inferno
        "Haart Lich",  // Necro
        "Mutare Drake",  // Dungeon
        "Boragus", "Kilgor",  // Stronghold
        "Adrienne",  // Fortress
        "Bidley", "Tark",  // Cove
    ],
    "banned": [
        "Sylvia", "Cuthbert", "Sanya", "Thorgrim", "Coronius", "Malcom",
        "Thane", "Iona", "Serena", "Aine", "Rashka", "Octavia", "Xyron", "Ash",
        "Nimbus", "Xsi", "Synca", "Geon", "Zubin", "Oris", "Voy", "Merist", "Tiva",
        "Kalt", "Sir Mullich", "Leena", "Astra", "Elmore"
    ]
};

const jr4_ludoclash_banned_heroes = get_standard_banned_heroes_list(jr4_ludoclash_heroes);
