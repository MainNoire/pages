function render_heroes(picked_heroes) {
    picked_heroes.forEach(function (hero_info) {
        let heroes_container = document.getElementById('heroes');

        let card = document.createElement('div');
        card.className = 'card';

        let img = document.createElement('div');
        img.className = 'ava';

        let image = document.createElement('img');
        image.setAttribute('src', hero_info.hero.img);
        img.appendChild(image);

        let town = document.createElement('div');
        town.className = 'town';

        let town_name = document.createElement('h3');
        town_name.appendChild(document.createTextNode(hero_info.class.town));
        town.appendChild(town_name);

        let town_image = document.createElement('img');
        town_image.setAttribute('src', towns[hero_info.class.town.toLowerCase()]['img']);
        town.appendChild(town_image);

        let hero = document.createElement('h2');
        hero.className = 'hero';
        hero.appendChild(document.createTextNode(hero_info.hero.name));

        let skills = document.createElement('div');
        skills.className = 'skills';

        let skill1 = document.createElement('img');
        skill1.setAttribute('src', hero_info.skill1.img);
        skill1.setAttribute('title', "Skill1: " + hero_info.skill1.name);
        skill1.setAttribute('alt', "Skill1: " + hero_info.skill1.name);
        skills.appendChild(skill1);
        if (hero_info.skill2 !== undefined) {
            let skill2 = document.createElement('img');
            skill2.setAttribute('src', hero_info.skill2.img);
            skill2.setAttribute('title', 'Skill2: ' + hero_info.skill2.name);
            skill2.setAttribute('alt', 'Skill2: ' + hero_info.skill2.name);
            skills.appendChild(skill2);
        }

        let specialty_and_spell = document.createElement('div');
        specialty_and_spell.className = 'specialty_and_spell';

        let specialty_img = document.createElement('img');
        specialty_img.setAttribute('src', hero_info.specialty.img);
        specialty_img.setAttribute('title', 'Specialty: ' + hero_info.specialty.name);
        specialty_img.setAttribute('alt', 'Specialty: ' + hero_info.specialty.name);
        specialty_and_spell.appendChild(specialty_img);

        if (hero_info.spell !== undefined) {
            let spell_img = document.createElement('img');
            spell_img.setAttribute('src', hero_info.spell.img);
            spell_img.setAttribute('title', 'Spell: ' + hero_info.spell.name);
            spell_img.setAttribute('alt', 'Spell: ' + hero_info.spell.name);
            specialty_and_spell.appendChild(spell_img);
        }

        card.appendChild(img);
        card.appendChild(hero);
        card.appendChild(specialty_and_spell);
        card.appendChild(skills);
        card.appendChild(town);
        heroes_container.appendChild(card);
    });
}