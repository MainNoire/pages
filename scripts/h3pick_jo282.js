const jo282_heroes = {
    "allowed": [
        "Thorgrim",
        "Adrienne",
        "Catherine",
        "Mutare Drake",
        "Boragus",
        "Xeron"
    ],
    "banned": [
        "Valeska", "Sanya",
        "Mephala", "Ivor", "Kyrre", "Coronius", "Malcom",
        "Thane", "Neela", "Torosar", "Fafner", "Iona", "Serena", "Cyra", "Aine",
        "Rashka", "Ignatius", "Octavia", "Calh", "Xyron", "Ash",
        "Galthran", "Septienna", "Nimbus", "Thant",
        "Gunnar", "Synca", "Shakti", "Alamar", "Jeddite", "Geon",
        "Gretchin", "Crag Hack", "Tyraxor", "Dessa", "Terek", "Gundula",
        "Oris", "Drakon", "Wystan", "Tazar", "Tiva", "Brissa",
        "Grindan",
        "Corkes", "Derek", "Anabel", "Cassiopeia", "Miriam",
        "Beatrice", "Giselle",
        "Sylvia", "Voy", "Elmore"
    ]
};

const jo282_banned_heroes = get_standard_banned_heroes_list(jo282_heroes);

function h3pick_jo282(n_players, banned_towns) {
    return h3pick(n_players, false, banned_towns, jo282_banned_heroes);
}
