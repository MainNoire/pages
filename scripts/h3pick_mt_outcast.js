const mt_outcast_heroes = {
    // Everybody welcome
    "allowed": [
        "Catherine", "Roland", "Sir Mullich",  // Castle
        "Gelu", "Thorgrim", // Rampart
        "Dracon",  // Tower
        "Xeron",  // Inferno
        "Haart Lich",  // Necro
        "Mutare Drake",  // Dungeon
        "Boragus", "Kilgor",  // Stronghold
        "Adrienne",  // Fortress
        "Bidley", "Tark",  // Cove
    ],
    "banned": [
        "Kyrre", "Gunnar", "Dessa", "Darkstorn", "Astra",
        "Ash", "Malcom", "Miriam", "Nimbus", "Oris",
        "Rion", "Sanya", "Serena", "Tiva", "Xsi",
        "Sorsha", "Sylvia", "Voy", "Elmore",
    ],
}

const banned_mt_outcast_heroes = get_standard_banned_heroes_list(mt_outcast_heroes)

function h3pick_mt_outcast(banned_town_list) {
    return h3pick(2, true, banned_town_list, banned_mt_outcast_heroes);
}
