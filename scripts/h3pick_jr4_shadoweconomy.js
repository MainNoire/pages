const jr4_shadoweconomy_heroes = {
    "allowed": [
        "Catherine", "Roland",  // Castle
        "Xeron",  // Inferno
        "Mutare Drake",  // Dungeon
        "Boragus",  // Stronghold
        "Adrienne",  // Fortress
        "Bidley",  // Cove
    ],
    "banned": [
        "Sylvia", "Cuthbert", "Sanya", "Mephala", "Thorgrim", "Kyrre",
        "Coronius", "Malcom", "Thane", "Iona", "Serena", "Cyra", "Aine",
        "Rashka", "Marius", "Octavia", "Calh", "Xyron", "Ash", "Nimbus",
        "Thant", "Xsi", "Gunnar", "Synca", "Alamar", "Jeddite", "Geon",
        "Deemer", "Shiva", "Crag Hack", "Dessa", "Terek", "Gundula", "Oris",
        "Tazar", "Broghild", "Voy", "Merist", "Tiva", "Kalt", "Brissa", "Grindan",
        "Sir Mullich", "Dracon", "Gelu", "Kilgor", "Haart Lich", "Corkes",
        "Miriam", "Eovacius", "Tark", "Elmore", "Beatrice", "Giselle"
    ]
};

const jr4_shadoweconomy_banned_heroes = get_standard_banned_heroes_list(jr4_shadoweconomy_heroes);
