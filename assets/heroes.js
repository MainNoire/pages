const heroes = [
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Anabel",
            "name": "Anabel",
            "img": "https://heroes.thelazy.net/images/1/1d/Hero_Anabel.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Pirates",
            "name": "Pirates",
            "img": "https://heroes.thelazy.net/images/1/11/Specialty_Pirates.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Cassiopeia",
            "name": "Cassiopeia",
            "img": "https://heroes.thelazy.net/images/d/d9/Hero_Cassiopeia.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Nymphs",
            "name": "Nymphs",
            "img": "https://heroes.thelazy.net/images/e/e1/Specialty_Nymphs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Corkes",
            "name": "Corkes",
            "img": "https://heroes.thelazy.net/images/c/c5/Hero_Corkes.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Offense",
            "name": "Offense",
            "img": "https://heroes.thelazy.net/images/b/b9/Specialty_Offense.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Derek",
            "name": "Derek",
            "img": "https://heroes.thelazy.net/images/b/bc/Hero_Derek.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Crew_Mates",
            "name": "Crew Mates",
            "img": "https://heroes.thelazy.net/images/b/b6/Specialty_Crew_Mates.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Elmore",
            "name": "Elmore",
            "img": "https://heroes.thelazy.net/images/9/9f/Hero_Elmore.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Navigation",
            "name": "Navigation",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Navigation.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Navigation",
            "name": "Advanced Navigation",
            "img": "https://heroes.thelazy.net/images/b/bf/Advanced_Navigation.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Illor",
            "name": "Illor",
            "img": "https://heroes.thelazy.net/images/e/e9/Hero_Illor.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Stormbirds",
            "name": "Stormbirds",
            "img": "https://heroes.thelazy.net/images/d/d4/Specialty_Stormbirds.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Jeremy",
            "name": "Jeremy",
            "img": "https://heroes.thelazy.net/images/9/9d/Hero_Jeremy.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Cannon",
            "name": "Cannon",
            "img": "https://heroes.thelazy.net/images/f/f8/Specialty_Cannon.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Leena",
            "name": "Leena",
            "img": "https://heroes.thelazy.net/images/0/0b/Hero_Leena.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Miriam",
            "name": "Miriam",
            "img": "https://heroes.thelazy.net/images/1/14/Hero_Miriam.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Scouting",
            "name": "Scouting",
            "img": "https://heroes.thelazy.net/images/e/e3/Specialty_Scouting.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Bidley",
            "name": "Bidley",
            "img": "https://heroes.thelazy.net/images/e/e8/Hero_Bidley.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sea_Dogs",
            "name": "Sea Dogs",
            "img": "https://heroes.thelazy.net/images/3/3a/Specialty_Sea_Dogs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tark",
            "name": "Tark",
            "img": "https://heroes.thelazy.net/images/6/66/Hero_Tark.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Captain",
            "name": "Captain",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Nix",
            "name": "Nix",
            "img": "https://heroes.thelazy.net/images/8/8f/Specialty_Nix.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Andal",
            "name": "Andal",
            "img": "https://heroes.thelazy.net/images/b/b3/Hero_Andal.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Crystal",
            "name": "Crystal",
            "img": "https://heroes.thelazy.net/images/6/6c/Specialty_Crystal.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Astra",
            "name": "Astra",
            "img": "https://heroes.thelazy.net/images/d/dc/Hero_Astra.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Cure",
            "name": "Cure",
            "img": "https://heroes.thelazy.net/images/7/7b/Cure.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Luck",
            "name": "Basic Luck",
            "img": "https://heroes.thelazy.net/images/f/fc/Basic_Luck.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Cure",
            "name": "Cure",
            "img": "https://heroes.thelazy.net/images/7/7b/Cure.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Casmetra",
            "name": "Casmetra",
            "img": "https://heroes.thelazy.net/images/0/09/Hero_Casmetra.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sea_Witches",
            "name": "Sea Witches",
            "img": "https://heroes.thelazy.net/images/c/c2/Specialty_Sea_Witches.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Water_Magic",
            "name": "Basic Water Magic",
            "img": "https://heroes.thelazy.net/images/5/59/Basic_Water_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Dispel",
            "name": "Dispel",
            "img": "https://heroes.thelazy.net/images/7/7a/Dispel.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Dargem",
            "name": "Dargem",
            "img": "https://heroes.thelazy.net/images/f/f0/Hero_Dargem.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Air_Shield",
            "name": "Air Shield",
            "img": "https://heroes.thelazy.net/images/d/d8/Air_Shield.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Air_Shield",
            "name": "Air Shield",
            "img": "https://heroes.thelazy.net/images/d/d8/Air_Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Eovacius",
            "name": "Eovacius",
            "img": "https://heroes.thelazy.net/images/3/3b/Hero_Eovacius.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Clone",
            "name": "Clone",
            "img": "https://heroes.thelazy.net/images/c/c9/Specialty_Clone.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Clone",
            "name": "Clone",
            "img": "https://heroes.thelazy.net/images/c/c9/Specialty_Clone.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Manfred",
            "name": "Manfred",
            "img": "https://heroes.thelazy.net/images/e/e4/Hero_Manfred.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fireball",
            "name": "Fireball",
            "img": "https://heroes.thelazy.net/images/a/ae/Fireball.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Fire_Magic",
            "name": "Basic Fire Magic",
            "img": "https://heroes.thelazy.net/images/0/00/Basic_Fire_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Fireball",
            "name": "Fireball",
            "img": "https://heroes.thelazy.net/images/a/ae/Fireball.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Spint",
            "name": "Spint",
            "img": "https://heroes.thelazy.net/images/1/1a/Hero_Spint.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bless",
            "name": "Bless",
            "img": "https://heroes.thelazy.net/images/5/51/Bless.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Zilare",
            "name": "Zilare",
            "img": "https://heroes.thelazy.net/images/9/91/Hero_Zilare.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Navigator",
            "name": "Navigator",
            "town": "Cove"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Forgetfulness",
            "name": "Forgetfulness",
            "img": "https://heroes.thelazy.net/images/f/f8/Forgetfulness.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Forgetfulness",
            "name": "Forgetfulness",
            "img": "https://heroes.thelazy.net/images/f/f8/Forgetfulness.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Christian",
            "name": "Christian",
            "img": "https://heroes.thelazy.net/images/f/f8/Hero_Christian.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Edric",
            "name": "Edric",
            "img": "https://heroes.thelazy.net/images/7/7f/Hero_Edric.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Griffins",
            "name": "Griffins",
            "img": "https://heroes.thelazy.net/images/e/e8/Specialty_Griffins.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Orrin",
            "name": "Orrin",
            "img": "https://heroes.thelazy.net/images/4/4a/Hero_Orrin.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Archery",
            "name": "Archery",
            "img": "https://heroes.thelazy.net/images/0/0e/Specialty_Archery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sorsha",
            "name": "Sorsha",
            "img": "https://heroes.thelazy.net/images/1/14/Hero_Sorsha.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Swordsmen",
            "name": "Swordsmen",
            "img": "https://heroes.thelazy.net/images/9/90/Specialty_Swordsmen.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sylvia",
            "name": "Sylvia",
            "img": "https://heroes.thelazy.net/images/5/53/Hero_Sylvia.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Navigation",
            "name": "Navigation",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Navigation.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Navigation",
            "name": "Basic Navigation",
            "img": "https://heroes.thelazy.net/images/f/f9/Basic_Navigation.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Valeska",
            "name": "Valeska",
            "img": "https://heroes.thelazy.net/images/2/20/Hero_Valeska_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Archers",
            "name": "Archers",
            "img": "https://heroes.thelazy.net/images/6/67/Specialty_Archers.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tyris",
            "name": "Tyris",
            "img": "https://heroes.thelazy.net/images/c/c1/Hero_Tyris.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Cavaliers",
            "name": "Cavaliers",
            "img": "https://heroes.thelazy.net/images/6/64/Specialty_Cavaliers.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Lord_Haart",
            "name": "Lord\u00a0Haart",
            "img": "https://heroes.thelazy.net/images/e/ed/Hero_Lord_Haart_Knight.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Estates",
            "name": "Estates",
            "img": "https://heroes.thelazy.net/images/8/8f/Specialty_Estates.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Catherine",
            "name": "Catherine",
            "img": "https://heroes.thelazy.net/images/5/53/Hero_Catherine.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Swordsmen",
            "name": "Swordsmen",
            "img": "https://heroes.thelazy.net/images/9/90/Specialty_Swordsmen.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Roland",
            "name": "Roland",
            "img": "https://heroes.thelazy.net/images/7/7f/Hero_Roland.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Swordsmen",
            "name": "Swordsmen",
            "img": "https://heroes.thelazy.net/images/9/90/Specialty_Swordsmen.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sir_Mullich",
            "name": "Sir Mullich",
            "img": "https://heroes.thelazy.net/images/9/9a/Hero_Sir_Mullich_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Speed",
            "name": "Speed",
            "img": "https://heroes.thelazy.net/images/c/ca/Specialty_Speed.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Leadership",
            "name": "Advanced Leadership",
            "img": "https://heroes.thelazy.net/images/f/f8/Advanced_Leadership.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Beatrice",
            "name": "Beatrice",
            "img": "https://heroes.thelazy.net/images/0/05/Hero_Beatrice.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Knight",
            "name": "Knight",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Scouting",
            "name": "Scouting",
            "img": "https://heroes.thelazy.net/images/e/e3/Specialty_Scouting.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Adela",
            "name": "Adela",
            "img": "https://heroes.thelazy.net/images/3/32/Hero_Adela.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Bless",
            "name": "Bless",
            "img": "https://heroes.thelazy.net/images/5/51/Bless.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Diplomacy",
            "name": "Basic Diplomacy",
            "img": "https://heroes.thelazy.net/images/3/38/Basic_Diplomacy.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bless",
            "name": "Bless",
            "img": "https://heroes.thelazy.net/images/5/51/Bless.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Adelaide",
            "name": "Adelaide",
            "img": "https://heroes.thelazy.net/images/b/b9/Hero_Adelaide_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Frost_Ring",
            "name": "Frost Ring",
            "img": "https://heroes.thelazy.net/images/9/99/Specialty_Frost_Ring.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Wisdom",
            "name": "Advanced Wisdom",
            "img": "https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Frost_Ring",
            "name": "Frost Ring",
            "img": "https://heroes.thelazy.net/images/9/99/Specialty_Frost_Ring.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Caitlin",
            "name": "Caitlin",
            "img": "https://heroes.thelazy.net/images/5/50/Hero_Caitlin_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Cure",
            "name": "Cure",
            "img": "https://heroes.thelazy.net/images/7/7b/Cure.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Cuthbert",
            "name": "Cuthbert",
            "img": "https://heroes.thelazy.net/images/2/24/Hero_Cuthbert.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ingham",
            "name": "Ingham",
            "img": "https://heroes.thelazy.net/images/d/dd/Hero_Ingham.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Monks",
            "name": "Monks",
            "img": "https://heroes.thelazy.net/images/f/fc/Specialty_Monks.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Curse",
            "name": "Curse",
            "img": "https://heroes.thelazy.net/images/8/85/Curse.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Loynis",
            "name": "Loynis",
            "img": "https://heroes.thelazy.net/images/8/86/Hero_Loynis.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Prayer",
            "name": "Prayer",
            "img": "https://heroes.thelazy.net/images/f/f9/Specialty_Prayer.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Prayer",
            "name": "Prayer",
            "img": "https://heroes.thelazy.net/images/f/f9/Specialty_Prayer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Rion",
            "name": "Rion",
            "img": "https://heroes.thelazy.net/images/d/d4/Hero_Rion.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/First_Aid",
            "name": "First Aid",
            "img": "https://heroes.thelazy.net/images/7/70/Specialty_First_Aid.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_First_Aid",
            "name": "Basic First Aid",
            "img": "https://heroes.thelazy.net/images/3/30/Basic_First_Aid.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sanya",
            "name": "Sanya",
            "img": "https://heroes.thelazy.net/images/9/93/Hero_Sanya.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Cleric",
            "name": "Cleric",
            "town": "Castle"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Dispel",
            "name": "Dispel",
            "img": "https://heroes.thelazy.net/images/7/7a/Dispel.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Clancy",
            "name": "Clancy",
            "img": "https://heroes.thelazy.net/images/6/62/Hero_Clancy.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Unicorns",
            "name": "Unicorns",
            "img": "https://heroes.thelazy.net/images/2/2e/Specialty_Unicorns.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ivor",
            "name": "Ivor",
            "img": "https://heroes.thelazy.net/images/2/2c/Hero_Ivor.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Elves",
            "name": "Elves",
            "img": "https://heroes.thelazy.net/images/f/f1/Specialty_Elves.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Jenova",
            "name": "Jenova",
            "img": "https://heroes.thelazy.net/images/e/ed/Hero_Jenova.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Archery",
            "name": "Advanced Archery",
            "img": "https://heroes.thelazy.net/images/2/25/Advanced_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Kyrre",
            "name": "Kyrre",
            "img": "https://heroes.thelazy.net/images/2/23/Hero_Kyrre.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Logistics",
            "name": "Logistics",
            "img": "https://heroes.thelazy.net/images/8/85/Specialty_Logistics.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Mephala",
            "name": "Mephala",
            "img": "https://heroes.thelazy.net/images/8/86/Hero_Mephala.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Armorer",
            "name": "Armorer",
            "img": "https://heroes.thelazy.net/images/3/36/Specialty_Armorer.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ryland",
            "name": "Ryland",
            "img": "https://heroes.thelazy.net/images/0/06/Hero_Ryland.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Dendroids",
            "name": "Dendroids",
            "img": "https://heroes.thelazy.net/images/0/0a/Specialty_Dendroids.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Diplomacy",
            "name": "Basic Diplomacy",
            "img": "https://heroes.thelazy.net/images/3/38/Basic_Diplomacy.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Thorgrim",
            "name": "Thorgrim",
            "img": "https://heroes.thelazy.net/images/c/ce/Hero_Thorgrim_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Resistance",
            "name": "Resistance",
            "img": "https://heroes.thelazy.net/images/6/68/Specialty_Resistance.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Resistance",
            "name": "Advanced Resistance",
            "img": "https://heroes.thelazy.net/images/f/f1/Advanced_Resistance.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ufretin",
            "name": "Ufretin",
            "img": "https://heroes.thelazy.net/images/0/05/Hero_Ufretin.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Dwarves",
            "name": "Dwarves",
            "img": "https://heroes.thelazy.net/images/a/ab/Specialty_Dwarves.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Luck",
            "name": "Basic Luck",
            "img": "https://heroes.thelazy.net/images/f/fc/Basic_Luck.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gelu",
            "name": "Gelu",
            "img": "https://heroes.thelazy.net/images/e/e2/Hero_Gelu.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sharpshooters",
            "name": "Sharpshooters",
            "img": "https://heroes.thelazy.net/images/f/fe/Specialty_Sharpshooters.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Giselle",
            "name": "Giselle",
            "img": "https://heroes.thelazy.net/images/e/e4/Hero_Giselle.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Ranger",
            "name": "Ranger",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Interference",
            "name": "Interference",
            "img": "https://heroes.thelazy.net/images/2/2c/Specialty_Interference.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Interference",
            "name": "Advanced Interference",
            "img": "https://heroes.thelazy.net/images/c/c4/Advanced_Interference.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Aeris",
            "name": "Aeris",
            "img": "https://heroes.thelazy.net/images/7/74/Hero_Aeris.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Pegasi",
            "name": "Pegasi",
            "img": "https://heroes.thelazy.net/images/5/51/Specialty_Pegasi.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Air",
            "name": "Protection from Air",
            "img": "https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Alagar",
            "name": "Alagar",
            "img": "https://heroes.thelazy.net/images/4/43/Hero_Alagar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ice_Bolt",
            "name": "Ice Bolt",
            "img": "https://heroes.thelazy.net/images/9/97/Ice_Bolt.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Ice_Bolt",
            "name": "Ice Bolt",
            "img": "https://heroes.thelazy.net/images/9/97/Ice_Bolt.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Coronius",
            "name": "Coronius",
            "img": "https://heroes.thelazy.net/images/e/ed/Hero_Coronius_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Slayer",
            "name": "Slayer",
            "img": "https://heroes.thelazy.net/images/f/f2/Specialty_Slayer.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slayer",
            "name": "Slayer",
            "img": "https://heroes.thelazy.net/images/f/f2/Specialty_Slayer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Elleshar",
            "name": "Elleshar",
            "img": "https://heroes.thelazy.net/images/b/b8/Hero_Elleshar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Intelligence",
            "name": "Intelligence",
            "img": "https://heroes.thelazy.net/images/7/74/Specialty_Intelligence.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Curse",
            "name": "Curse",
            "img": "https://heroes.thelazy.net/images/8/85/Curse.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gem",
            "name": "Gem",
            "img": "https://heroes.thelazy.net/images/9/9f/Hero_Gem_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/First_Aid",
            "name": "First Aid",
            "img": "https://heroes.thelazy.net/images/7/70/Specialty_First_Aid.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_First_Aid",
            "name": "Basic First Aid",
            "img": "https://heroes.thelazy.net/images/3/30/Basic_First_Aid.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Summon_Boat",
            "name": "Summon Boat",
            "img": "https://heroes.thelazy.net/images/b/b0/Summon_Boat.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Malcom",
            "name": "Malcom",
            "img": "https://heroes.thelazy.net/images/2/22/Hero_Malcom.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Melodia",
            "name": "Melodia",
            "img": "https://heroes.thelazy.net/images/4/4b/Hero_Melodia.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fortune",
            "name": "Fortune",
            "img": "https://heroes.thelazy.net/images/b/bf/Fortune.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Luck",
            "name": "Basic Luck",
            "img": "https://heroes.thelazy.net/images/f/fc/Basic_Luck.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Fortune",
            "name": "Fortune",
            "img": "https://heroes.thelazy.net/images/b/bf/Fortune.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Uland",
            "name": "Uland",
            "img": "https://heroes.thelazy.net/images/e/ee/Hero_Uland.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Druid",
            "name": "Druid",
            "town": "Rampart"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Cure",
            "name": "Cure",
            "img": "https://heroes.thelazy.net/images/7/7b/Cure.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Ballistics",
            "name": "Basic Ballistics",
            "img": "https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Cure",
            "name": "Cure",
            "img": "https://heroes.thelazy.net/images/7/7b/Cure.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Fafner",
            "name": "Fafner",
            "img": "https://heroes.thelazy.net/images/d/d0/Hero_Fafner.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Nagas",
            "name": "Nagas",
            "img": "https://heroes.thelazy.net/images/2/2c/Specialty_Nagas.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Iona",
            "name": "Iona",
            "img": "https://heroes.thelazy.net/images/5/5c/Hero_Iona_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Genies",
            "name": "Genies",
            "img": "https://heroes.thelazy.net/images/0/0c/Specialty_Genies.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Josephine",
            "name": "Josephine",
            "img": "https://heroes.thelazy.net/images/6/62/Hero_Josephine.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Golems",
            "name": "Golems",
            "img": "https://heroes.thelazy.net/images/d/de/Specialty_Golems.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Neela",
            "name": "Neela",
            "img": "https://heroes.thelazy.net/images/8/80/Hero_Neela.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Armorer",
            "name": "Armorer",
            "img": "https://heroes.thelazy.net/images/3/36/Specialty_Armorer.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Piquedram",
            "name": "Piquedram",
            "img": "https://heroes.thelazy.net/images/4/42/Hero_Piquedram.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gargoyles",
            "name": "Gargoyles",
            "img": "https://heroes.thelazy.net/images/c/c3/Specialty_Gargoyles.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Rissa",
            "name": "Rissa",
            "img": "https://heroes.thelazy.net/images/7/7f/Hero_Rissa.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Mercury",
            "name": "Mercury",
            "img": "https://heroes.thelazy.net/images/2/2a/Specialty_Mercury.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Thane",
            "name": "Thane",
            "img": "https://heroes.thelazy.net/images/d/d6/Hero_Thane.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Genies",
            "name": "Genies",
            "img": "https://heroes.thelazy.net/images/0/0c/Specialty_Genies.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Scholar",
            "name": "Advanced Scholar",
            "img": "https://heroes.thelazy.net/images/2/24/Advanced_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Torosar",
            "name": "Torosar",
            "img": "https://heroes.thelazy.net/images/b/b3/Hero_Torosar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Alchemist",
            "name": "Alchemist",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Aine",
            "name": "Aine",
            "img": "https://heroes.thelazy.net/images/c/cc/Hero_Aine.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Curse",
            "name": "Curse",
            "img": "https://heroes.thelazy.net/images/8/85/Curse.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Astral",
            "name": "Astral",
            "img": "https://heroes.thelazy.net/images/3/3a/Hero_Astral.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Hypnotize",
            "name": "Hypnotize",
            "img": "https://heroes.thelazy.net/images/7/77/Specialty_Hypnotize.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Wisdom",
            "name": "Advanced Wisdom",
            "img": "https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Hypnotize",
            "name": "Hypnotize",
            "img": "https://heroes.thelazy.net/images/7/77/Specialty_Hypnotize.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Cyra",
            "name": "Cyra",
            "img": "https://heroes.thelazy.net/images/4/45/Hero_Cyra.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Diplomacy",
            "name": "Basic Diplomacy",
            "img": "https://heroes.thelazy.net/images/3/38/Basic_Diplomacy.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Daremyth",
            "name": "Daremyth",
            "img": "https://heroes.thelazy.net/images/0/0d/Hero_Daremyth.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fortune",
            "name": "Fortune",
            "img": "https://heroes.thelazy.net/images/b/bf/Fortune.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Fortune",
            "name": "Fortune",
            "img": "https://heroes.thelazy.net/images/b/bf/Fortune.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Halon",
            "name": "Halon",
            "img": "https://heroes.thelazy.net/images/e/ec/Hero_Halon.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Mysticism",
            "name": "Mysticism",
            "img": "https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Serena",
            "name": "Serena",
            "img": "https://heroes.thelazy.net/images/7/77/Hero_Serena.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Dispel",
            "name": "Dispel",
            "img": "https://heroes.thelazy.net/images/7/7a/Dispel.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Solmyr",
            "name": "Solmyr",
            "img": "https://heroes.thelazy.net/images/9/96/Hero_Solmyr.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Chain_Lightning",
            "name": "Chain Lightning",
            "img": "https://heroes.thelazy.net/images/e/ec/Specialty_Chain_Lightning.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Chain_Lightning",
            "name": "Chain Lightning",
            "img": "https://heroes.thelazy.net/images/e/ec/Specialty_Chain_Lightning.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Theodorus",
            "name": "Theodorus",
            "img": "https://heroes.thelazy.net/images/9/99/Hero_Theodorus.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Magi",
            "name": "Magi",
            "img": "https://heroes.thelazy.net/images/2/2e/Specialty_Magi.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Ballistics",
            "name": "Basic Ballistics",
            "img": "https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Dracon",
            "name": "Dracon",
            "img": "https://heroes.thelazy.net/images/c/c6/Hero_Dracon.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Wizard",
            "name": "Wizard",
            "town": "Tower"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Enchanters",
            "name": "Enchanters",
            "img": "https://heroes.thelazy.net/images/e/e5/Specialty_Enchanters.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Wisdom",
            "name": "Advanced Wisdom",
            "img": "https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Calh",
            "name": "Calh",
            "img": "https://heroes.thelazy.net/images/7/72/Hero_Calh.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gogs",
            "name": "Gogs",
            "img": "https://heroes.thelazy.net/images/7/7e/Specialty_Gogs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Fiona",
            "name": "Fiona",
            "img": "https://heroes.thelazy.net/images/6/60/Hero_Fiona.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Hell_Hounds",
            "name": "Hell Hounds",
            "img": "https://heroes.thelazy.net/images/4/4e/Specialty_Hell_Hounds.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Scouting",
            "name": "Advanced Scouting",
            "img": "https://heroes.thelazy.net/images/0/0e/Advanced_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ignatius",
            "name": "Ignatius",
            "img": "https://heroes.thelazy.net/images/8/8d/Hero_Ignatius.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Imps",
            "name": "Imps",
            "img": "https://heroes.thelazy.net/images/5/59/Specialty_Imps.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Marius",
            "name": "Marius",
            "img": "https://heroes.thelazy.net/images/9/92/Hero_Marius_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Demons",
            "name": "Demons",
            "img": "https://heroes.thelazy.net/images/2/25/Specialty_Demons.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Armorer",
            "name": "Advanced Armorer",
            "img": "https://heroes.thelazy.net/images/0/06/Advanced_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Nymus",
            "name": "Nymus",
            "img": "https://heroes.thelazy.net/images/d/d2/Hero_Nymus_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Pit_Fiends",
            "name": "Pit Fiends",
            "img": "https://heroes.thelazy.net/images/e/ee/Specialty_Pit_Fiends.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Octavia",
            "name": "Octavia",
            "img": "https://heroes.thelazy.net/images/2/2a/Hero_Octavia.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Pyre",
            "name": "Pyre",
            "img": "https://heroes.thelazy.net/images/1/16/Hero_Pyre.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Rashka",
            "name": "Rashka",
            "img": "https://heroes.thelazy.net/images/9/93/Hero_Rashka.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Efreet",
            "name": "Efreet",
            "img": "https://heroes.thelazy.net/images/2/29/Specialty_Efreet.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Xeron",
            "name": "Xeron",
            "img": "https://heroes.thelazy.net/images/8/89/Hero_Xeron.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Demoniac",
            "name": "Demoniac",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Devils",
            "name": "Devils",
            "img": "https://heroes.thelazy.net/images/c/c4/Specialty_Devils.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ash",
            "name": "Ash",
            "img": "https://heroes.thelazy.net/images/a/a2/Hero_Ash.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Axsis",
            "name": "Axsis",
            "img": "https://heroes.thelazy.net/images/f/f2/Hero_Axsis.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Mysticism",
            "name": "Mysticism",
            "img": "https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Air",
            "name": "Protection from Air",
            "img": "https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ayden",
            "name": "Ayden",
            "img": "https://heroes.thelazy.net/images/3/36/Hero_Ayden.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Intelligence",
            "name": "Intelligence",
            "img": "https://heroes.thelazy.net/images/7/74/Specialty_Intelligence.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/View_Earth",
            "name": "View Earth",
            "img": "https://heroes.thelazy.net/images/f/f9/View_Earth.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Calid",
            "name": "Calid",
            "img": "https://heroes.thelazy.net/images/c/cf/Hero_Calid.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sulfur",
            "name": "Sulfur",
            "img": "https://heroes.thelazy.net/images/8/87/Specialty_Sulfur.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Olema",
            "name": "Olema",
            "img": "https://heroes.thelazy.net/images/e/e7/Hero_Olema_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Ballistics",
            "name": "Basic Ballistics",
            "img": "https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Xarfax",
            "name": "Xarfax",
            "img": "https://heroes.thelazy.net/images/4/40/Hero_Xarfax.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fireball",
            "name": "Fireball",
            "img": "https://heroes.thelazy.net/images/a/ae/Fireball.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Fireball",
            "name": "Fireball",
            "img": "https://heroes.thelazy.net/images/a/ae/Fireball.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Xyron",
            "name": "Xyron",
            "img": "https://heroes.thelazy.net/images/3/3b/Hero_Xyron.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Inferno_(spell)",
            "name": "Inferno (spell)",
            "img": "https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Inferno_(spell)",
            "name": "Inferno (spell)",
            "img": "https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Zydar",
            "name": "Zydar",
            "img": "https://heroes.thelazy.net/images/9/98/Hero_Zydar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Heretic",
            "name": "Heretic",
            "town": "Inferno"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Charna",
            "name": "Charna",
            "img": "https://heroes.thelazy.net/images/a/a6/Hero_Charna.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Wights",
            "name": "Wights",
            "img": "https://heroes.thelazy.net/images/2/25/Specialty_Wights.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Clavius",
            "name": "Clavius",
            "img": "https://heroes.thelazy.net/images/6/61/Hero_Clavius.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Galthran",
            "name": "Galthran",
            "img": "https://heroes.thelazy.net/images/7/7e/Hero_Galthran_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Skeletons",
            "name": "Skeletons",
            "img": "https://heroes.thelazy.net/images/d/d2/Specialty_Skeletons.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Isra",
            "name": "Isra",
            "img": "https://heroes.thelazy.net/images/9/91/Hero_Isra.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Necromancy",
            "name": "Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6f/Specialty_Necromancy.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Necromancy",
            "name": "Advanced Necromancy",
            "img": "https://heroes.thelazy.net/images/7/7f/Advanced_Necromancy.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Moandor",
            "name": "Moandor",
            "img": "https://heroes.thelazy.net/images/4/41/Hero_Moandor.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Liches",
            "name": "Liches",
            "img": "https://heroes.thelazy.net/images/c/cb/Specialty_Liches.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Straker",
            "name": "Straker",
            "img": "https://heroes.thelazy.net/images/e/e9/Hero_Straker.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Walking_Dead",
            "name": "Walking Dead",
            "img": "https://heroes.thelazy.net/images/0/00/Specialty_Walking_Dead.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tamika",
            "name": "Tamika",
            "img": "https://heroes.thelazy.net/images/0/05/Hero_Tamika.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Black_Knights",
            "name": "Black Knights",
            "img": "https://heroes.thelazy.net/images/c/c5/Specialty_Black_Knights.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Vokial",
            "name": "Vokial",
            "img": "https://heroes.thelazy.net/images/4/4a/Hero_Vokial.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Vampires",
            "name": "Vampires",
            "img": "https://heroes.thelazy.net/images/a/aa/Specialty_Vampires.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Lord_Haart_the_Death_Knight",
            "name": "Haart Lich",
            "img": "https://heroes.thelazy.net/images/2/26/Hero_Lord_Haart_Death_Knight.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Black_Knights",
            "name": "Black Knights",
            "img": "https://heroes.thelazy.net/images/c/c5/Specialty_Black_Knights.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Necromancy",
            "name": "Advanced Necromancy",
            "img": "https://heroes.thelazy.net/images/7/7f/Advanced_Necromancy.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ranloo",
            "name": "Ranloo",
            "img": "https://heroes.thelazy.net/images/2/2f/Hero_Ranloo.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Death_Knight",
            "name": "Death Knight",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Aislinn",
            "name": "Aislinn",
            "img": "https://heroes.thelazy.net/images/2/25/Hero_Aislinn.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Meteor_Shower",
            "name": "Meteor Shower",
            "img": "https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Meteor_Shower",
            "name": "Meteor Shower",
            "img": "https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Nagash",
            "name": "Nagash",
            "img": "https://heroes.thelazy.net/images/6/6b/Hero_Nagash.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Air",
            "name": "Protection from Air",
            "img": "https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Nimbus",
            "name": "Nimbus",
            "img": "https://heroes.thelazy.net/images/c/c8/Hero_Nimbus_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sandro",
            "name": "Sandro",
            "img": "https://heroes.thelazy.net/images/1/1a/Hero_Sandro.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Septienna",
            "name": "Septienna",
            "img": "https://heroes.thelazy.net/images/7/72/Hero_Septienna.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Death_Ripple",
            "name": "Death Ripple",
            "img": "https://heroes.thelazy.net/images/3/3f/Death_Ripple.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Death_Ripple",
            "name": "Death Ripple",
            "img": "https://heroes.thelazy.net/images/3/3f/Death_Ripple.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Thant",
            "name": "Thant",
            "img": "https://heroes.thelazy.net/images/a/a1/Hero_Thant.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Animate_Dead",
            "name": "Animate Dead",
            "img": "https://heroes.thelazy.net/images/f/f8/Animate_Dead.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Animate_Dead",
            "name": "Animate Dead",
            "img": "https://heroes.thelazy.net/images/f/f8/Animate_Dead.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Vidomina",
            "name": "Vidomina",
            "img": "https://heroes.thelazy.net/images/6/6a/Hero_Vidomina.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Necromancy",
            "name": "Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6f/Specialty_Necromancy.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Necromancy",
            "name": "Advanced Necromancy",
            "img": "https://heroes.thelazy.net/images/7/7f/Advanced_Necromancy.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Curse",
            "name": "Curse",
            "img": "https://heroes.thelazy.net/images/8/85/Curse.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Xsi",
            "name": "Xsi",
            "img": "https://heroes.thelazy.net/images/8/85/Hero_Xsi.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Necromancer",
            "name": "Necromancer",
            "town": "Necropolis"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Necromancy",
            "name": "Basic Necromancy",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Necromancy.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ajit",
            "name": "Ajit",
            "img": "https://heroes.thelazy.net/images/c/c9/Hero_Ajit_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Beholders",
            "name": "Beholders",
            "img": "https://heroes.thelazy.net/images/3/31/Specialty_Beholders.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Arlach",
            "name": "Arlach",
            "img": "https://heroes.thelazy.net/images/a/ae/Hero_Arlach.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Dace",
            "name": "Dace",
            "img": "https://heroes.thelazy.net/images/a/aa/Hero_Dace.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Minotaurs",
            "name": "Minotaurs",
            "img": "https://heroes.thelazy.net/images/9/96/Specialty_Minotaurs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Damacon",
            "name": "Damacon",
            "img": "https://heroes.thelazy.net/images/5/5b/Hero_Damacon.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gunnar",
            "name": "Gunnar",
            "img": "https://heroes.thelazy.net/images/a/af/Hero_Gunnar_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Logistics",
            "name": "Logistics",
            "img": "https://heroes.thelazy.net/images/8/85/Specialty_Logistics.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Lorelei",
            "name": "Lorelei",
            "img": "https://heroes.thelazy.net/images/e/ea/Hero_Lorelei.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Harpies",
            "name": "Harpies",
            "img": "https://heroes.thelazy.net/images/a/a2/Specialty_Harpies.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Shakti",
            "name": "Shakti",
            "img": "https://heroes.thelazy.net/images/1/18/Hero_Shakti.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Troglodytes",
            "name": "Troglodytes",
            "img": "https://heroes.thelazy.net/images/8/88/Specialty_Troglodytes.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Synca",
            "name": "Synca",
            "img": "https://heroes.thelazy.net/images/9/93/Hero_Synca.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Manticores",
            "name": "Manticores",
            "img": "https://heroes.thelazy.net/images/3/3b/Specialty_Manticores.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Mutare",
            "name": "Mutare",
            "img": "https://heroes.thelazy.net/images/6/68/Hero_Mutare.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Dragons",
            "name": "Dragons",
            "img": "https://heroes.thelazy.net/images/7/75/Specialty_Dragons.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Mutare_Drake",
            "name": "Mutare Drake",
            "img": "https://heroes.thelazy.net/images/e/e4/Hero_Mutare_Drake.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Overlord",
            "name": "Overlord",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Dragons",
            "name": "Dragons",
            "img": "https://heroes.thelazy.net/images/7/75/Specialty_Dragons.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Alamar",
            "name": "Alamar",
            "img": "https://heroes.thelazy.net/images/5/5c/Hero_Alamar_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Resurrection",
            "name": "Resurrection",
            "img": "https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scholar",
            "name": "Basic Scholar",
            "img": "https://heroes.thelazy.net/images/2/2c/Basic_Scholar.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Resurrection",
            "name": "Resurrection",
            "img": "https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Darkstorn",
            "name": "Darkstorn",
            "img": "https://heroes.thelazy.net/images/8/87/Hero_Darkstorn.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Deemer",
            "name": "Deemer",
            "img": "https://heroes.thelazy.net/images/f/fe/Hero_Deemer.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Meteor_Shower",
            "name": "Meteor Shower",
            "img": "https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Meteor_Shower",
            "name": "Meteor Shower",
            "img": "https://heroes.thelazy.net/images/6/6b/Specialty_Meteor_Shower.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Geon",
            "name": "Geon",
            "img": "https://heroes.thelazy.net/images/4/48/Hero_Geon.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Jaegar",
            "name": "Jaegar",
            "img": "https://heroes.thelazy.net/images/9/93/Hero_Jaegar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Mysticism",
            "name": "Mysticism",
            "img": "https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Jeddite",
            "name": "Jeddite",
            "img": "https://heroes.thelazy.net/images/c/c9/Hero_Jeddite.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Resurrection",
            "name": "Resurrection",
            "img": "https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Wisdom",
            "name": "Advanced Wisdom",
            "img": "https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Resurrection",
            "name": "Resurrection",
            "img": "https://heroes.thelazy.net/images/b/b0/Specialty_Resurrection.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Malekith",
            "name": "Malekith",
            "img": "https://heroes.thelazy.net/images/5/5d/Hero_Malekith.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Sephinroth",
            "name": "Sephinroth",
            "img": "https://heroes.thelazy.net/images/1/1e/Hero_Sephinroth_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Warlock",
            "name": "Warlock",
            "town": "Dungeon"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Crystal",
            "name": "Crystal",
            "img": "https://heroes.thelazy.net/images/6/6c/Specialty_Crystal.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Air",
            "name": "Protection from Air",
            "img": "https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Crag_Hack",
            "name": "Crag Hack",
            "img": "https://heroes.thelazy.net/images/7/7e/Hero_Crag_Hack.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Offense",
            "name": "Offense",
            "img": "https://heroes.thelazy.net/images/b/b9/Specialty_Offense.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gretchin",
            "name": "Gretchin",
            "img": "https://heroes.thelazy.net/images/2/22/Hero_Gretchin_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Goblins",
            "name": "Goblins",
            "img": "https://heroes.thelazy.net/images/c/c9/Specialty_Goblins.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gurnisson",
            "name": "Gurnisson",
            "img": "https://heroes.thelazy.net/images/5/5d/Hero_Gurnisson_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Jabarkas",
            "name": "Jabarkas",
            "img": "https://heroes.thelazy.net/images/f/fb/Hero_Jabarkas.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Orcs",
            "name": "Orcs",
            "img": "https://heroes.thelazy.net/images/1/14/Specialty_Orcs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Krellion",
            "name": "Krellion",
            "img": "https://heroes.thelazy.net/images/f/f9/Hero_Krellion.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ogres",
            "name": "Ogres",
            "img": "https://heroes.thelazy.net/images/9/92/Specialty_Ogres.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Shiva",
            "name": "Shiva",
            "img": "https://heroes.thelazy.net/images/5/5d/Hero_Shiva.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Rocs",
            "name": "Rocs",
            "img": "https://heroes.thelazy.net/images/4/42/Specialty_Rocs.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tyraxor",
            "name": "Tyraxor",
            "img": "https://heroes.thelazy.net/images/b/b6/Hero_Tyraxor_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Wolf_Riders",
            "name": "Wolf Riders",
            "img": "https://heroes.thelazy.net/images/4/48/Specialty_Wolf_Riders.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Yog",
            "name": "Yog",
            "img": "https://heroes.thelazy.net/images/3/39/Hero_Yog.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Cyclops",
            "name": "Cyclops",
            "img": "https://heroes.thelazy.net/images/d/d5/Specialty_Cyclops.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Ballistics",
            "name": "Basic Ballistics",
            "img": "https://heroes.thelazy.net/images/d/da/Basic_Ballistics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Boragus",
            "name": "Boragus",
            "img": "https://heroes.thelazy.net/images/7/72/Hero_Boragus.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ogres",
            "name": "Ogres",
            "img": "https://heroes.thelazy.net/images/9/92/Specialty_Ogres.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Kilgor",
            "name": "Kilgor",
            "img": "https://heroes.thelazy.net/images/2/2b/Hero_Kilgor.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Barbarian",
            "name": "Barbarian",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Behemoths",
            "name": "Behemoths",
            "img": "https://heroes.thelazy.net/images/3/33/Specialty_Behemoths.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Dessa",
            "name": "Dessa",
            "img": "https://heroes.thelazy.net/images/c/c9/Hero_Dessa.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Logistics",
            "name": "Logistics",
            "img": "https://heroes.thelazy.net/images/8/85/Specialty_Logistics.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gird",
            "name": "Gird",
            "img": "https://heroes.thelazy.net/images/8/8b/Hero_Gird.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gundula",
            "name": "Gundula",
            "img": "https://heroes.thelazy.net/images/e/ea/Hero_Gundula.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Offense",
            "name": "Offense",
            "img": "https://heroes.thelazy.net/images/b/b9/Specialty_Offense.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Oris",
            "name": "Oris",
            "img": "https://heroes.thelazy.net/images/3/3a/Hero_Oris.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Air",
            "name": "Protection from Air",
            "img": "https://heroes.thelazy.net/images/a/a9/Protection_from_Air.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Saurug",
            "name": "Saurug",
            "img": "https://heroes.thelazy.net/images/d/d7/Hero_Saurug.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gems",
            "name": "Gems",
            "img": "https://heroes.thelazy.net/images/4/41/Specialty_Gems.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Terek",
            "name": "Terek",
            "img": "https://heroes.thelazy.net/images/3/3f/Hero_Terek.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Vey",
            "name": "Vey",
            "img": "https://heroes.thelazy.net/images/0/06/Hero_Vey_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ogres",
            "name": "Ogres",
            "img": "https://heroes.thelazy.net/images/9/92/Specialty_Ogres.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Zubin",
            "name": "Zubin",
            "img": "https://heroes.thelazy.net/images/1/1b/Hero_Zubin_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Battle_Mage",
            "name": "Battle Mage",
            "town": "Stronghold"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Precision",
            "name": "Precision",
            "img": "https://heroes.thelazy.net/images/a/af/Specialty_Precision.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Precision",
            "name": "Precision",
            "img": "https://heroes.thelazy.net/images/a/af/Specialty_Precision.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Alkin",
            "name": "Alkin",
            "img": "https://heroes.thelazy.net/images/a/ad/Hero_Alkin.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gorgons",
            "name": "Gorgons",
            "img": "https://heroes.thelazy.net/images/c/c1/Specialty_Gorgons.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Broghild",
            "name": "Broghild",
            "img": "https://heroes.thelazy.net/images/f/fd/Hero_Broghild.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Wyverns",
            "name": "Wyverns",
            "img": "https://heroes.thelazy.net/images/d/d5/Specialty_Wyverns.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Scouting",
            "name": "Basic Scouting",
            "img": "https://heroes.thelazy.net/images/7/7e/Basic_Scouting.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Bron",
            "name": "Bron",
            "img": "https://heroes.thelazy.net/images/8/82/Hero_Bron.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Basilisks",
            "name": "Basilisks",
            "img": "https://heroes.thelazy.net/images/d/d0/Specialty_Basilisks.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Interference",
            "name": "Basic Interference",
            "img": "https://heroes.thelazy.net/images/6/60/Basic_Interference.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Drakon",
            "name": "Drakon",
            "img": "https://heroes.thelazy.net/images/7/74/Hero_Drakon.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gnolls",
            "name": "Gnolls",
            "img": "https://heroes.thelazy.net/images/9/9e/Specialty_Gnolls.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Leadership",
            "name": "Basic Leadership",
            "img": "https://heroes.thelazy.net/images/9/9d/Basic_Leadership.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gerwulf",
            "name": "Gerwulf",
            "img": "https://heroes.thelazy.net/images/3/39/Hero_Gerwulf.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Ballista",
            "name": "Ballista",
            "img": "https://heroes.thelazy.net/images/1/16/Specialty_Ballista.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Korbac",
            "name": "Korbac",
            "img": "https://heroes.thelazy.net/images/6/6b/Hero_Korbac.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Serpent_Flies",
            "name": "Serpent Flies",
            "img": "https://heroes.thelazy.net/images/6/66/Specialty_Serpent_Flies.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Pathfinding",
            "name": "Basic Pathfinding",
            "img": "https://heroes.thelazy.net/images/7/72/Basic_Pathfinding.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tazar",
            "name": "Tazar",
            "img": "https://heroes.thelazy.net/images/d/dc/Hero_Tazar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Armorer",
            "name": "Armorer",
            "img": "https://heroes.thelazy.net/images/3/36/Specialty_Armorer.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Armorer",
            "name": "Advanced Armorer",
            "img": "https://heroes.thelazy.net/images/0/06/Advanced_Armorer.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Wystan",
            "name": "Wystan",
            "img": "https://heroes.thelazy.net/images/3/33/Hero_Wystan.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Beastmaster",
            "name": "Beastmaster",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Lizardmen",
            "name": "Lizardmen",
            "img": "https://heroes.thelazy.net/images/0/07/Specialty_Lizardmen.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Armorer",
            "name": "Basic Armorer",
            "img": "https://heroes.thelazy.net/images/9/99/Basic_Armorer.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Archery",
            "name": "Basic Archery",
            "img": "https://heroes.thelazy.net/images/6/6b/Basic_Archery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Andra",
            "name": "Andra",
            "img": "https://heroes.thelazy.net/images/6/6c/Hero_Andra.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Intelligence",
            "name": "Intelligence",
            "img": "https://heroes.thelazy.net/images/7/74/Specialty_Intelligence.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Intelligence",
            "name": "Basic Intelligence",
            "img": "https://heroes.thelazy.net/images/8/8c/Basic_Intelligence.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Dispel",
            "name": "Dispel",
            "img": "https://heroes.thelazy.net/images/7/7a/Dispel.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Merist",
            "name": "Merist",
            "img": "https://heroes.thelazy.net/images/a/a6/Hero_Merist_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Mirlanda",
            "name": "Mirlanda",
            "img": "https://heroes.thelazy.net/images/f/fe/Hero_Mirlanda.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Wisdom",
            "name": "Advanced Wisdom",
            "img": "https://heroes.thelazy.net/images/5/52/Advanced_Wisdom.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Weakness",
            "name": "Weakness",
            "img": "https://heroes.thelazy.net/images/e/e0/Specialty_Weakness.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Rosic",
            "name": "Rosic",
            "img": "https://heroes.thelazy.net/images/6/6f/Hero_Rosic.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Mysticism",
            "name": "Mysticism",
            "img": "https://heroes.thelazy.net/images/6/6a/Specialty_Mysticism.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Mysticism",
            "name": "Basic Mysticism",
            "img": "https://heroes.thelazy.net/images/1/12/Basic_Mysticism.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Styg",
            "name": "Styg",
            "img": "https://heroes.thelazy.net/images/7/7e/Hero_Styg.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Sorcery",
            "name": "Sorcery",
            "img": "https://heroes.thelazy.net/images/e/e2/Specialty_Sorcery.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Sorcery",
            "name": "Basic Sorcery",
            "img": "https://heroes.thelazy.net/images/a/ab/Basic_Sorcery.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Shield",
            "name": "Shield",
            "img": "https://heroes.thelazy.net/images/c/cf/Shield.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Tiva",
            "name": "Tiva",
            "img": "https://heroes.thelazy.net/images/3/3d/Hero_Tiva_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Eagle_Eye",
            "name": "Eagle Eye",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Eagle_Eye.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Eagle_Eye",
            "name": "Basic Eagle Eye",
            "img": "https://heroes.thelazy.net/images/e/eb/Basic_Eagle_Eye.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Verdish",
            "name": "Verdish",
            "img": "https://heroes.thelazy.net/images/1/1a/Hero_Verdish.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/First_Aid",
            "name": "First Aid",
            "img": "https://heroes.thelazy.net/images/7/70/Specialty_First_Aid.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_First_Aid",
            "name": "Basic First Aid",
            "img": "https://heroes.thelazy.net/images/3/30/Basic_First_Aid.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Protection_from_Fire",
            "name": "Protection from Fire",
            "img": "https://heroes.thelazy.net/images/a/a1/Protection_from_Fire.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Voy",
            "name": "Voy",
            "img": "https://heroes.thelazy.net/images/b/ba/Hero_Voy.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Navigation",
            "name": "Navigation",
            "img": "https://heroes.thelazy.net/images/c/cc/Specialty_Navigation.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Navigation",
            "name": "Basic Navigation",
            "img": "https://heroes.thelazy.net/images/f/f9/Basic_Navigation.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Adrienne",
            "name": "Adrienne",
            "img": "https://heroes.thelazy.net/images/1/1d/Hero_Adrienne.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fire_Magic",
            "name": "Fire Magic",
            "img": "https://heroes.thelazy.net/images/c/ce/Specialty_Fire_Magic.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Expert_Fire_Magic",
            "name": "Expert Fire Magic",
            "img": "https://heroes.thelazy.net/images/8/85/Expert_Fire_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Inferno_(spell)",
            "name": "Inferno (spell)",
            "img": "https://heroes.thelazy.net/images/0/0e/Specialty_Inferno_%28spell%29.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Kinkeria",
            "name": "Kinkeria",
            "img": "https://heroes.thelazy.net/images/0/0f/Hero_Kinkeria.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Witch",
            "name": "Witch",
            "town": "Fortress"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Learning",
            "name": "Learning",
            "img": "https://heroes.thelazy.net/images/c/c6/Specialty_Learning.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Erdamon",
            "name": "Erdamon",
            "img": "https://heroes.thelazy.net/images/3/3b/Hero_Erdamon_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Earth_Elementals",
            "name": "Earth Elementals",
            "img": "https://heroes.thelazy.net/images/f/fa/Specialty_Earth_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Fiur",
            "name": "Fiur",
            "img": "https://heroes.thelazy.net/images/0/0e/Hero_Fiur.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fire_Elementals",
            "name": "Fire Elementals",
            "img": "https://heroes.thelazy.net/images/8/8e/Specialty_Fire_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Offense",
            "name": "Advanced Offense",
            "img": "https://heroes.thelazy.net/images/2/22/Advanced_Offense.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ignissa",
            "name": "Ignissa",
            "img": "https://heroes.thelazy.net/images/a/a3/Hero_Ignissa.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fire_Elementals",
            "name": "Fire Elementals",
            "img": "https://heroes.thelazy.net/images/8/8e/Specialty_Fire_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Kalt",
            "name": "Kalt",
            "img": "https://heroes.thelazy.net/images/c/c9/Hero_Kalt_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Water_Elementals",
            "name": "Water Elementals",
            "img": "https://heroes.thelazy.net/images/e/e5/Specialty_Water_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Learning",
            "name": "Basic Learning",
            "img": "https://heroes.thelazy.net/images/a/a4/Basic_Learning.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Lacus",
            "name": "Lacus",
            "img": "https://heroes.thelazy.net/images/9/9f/Hero_Lacus.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Water_Elementals",
            "name": "Water Elementals",
            "img": "https://heroes.thelazy.net/images/e/e5/Specialty_Water_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Advanced_Tactics",
            "name": "Advanced Tactics",
            "img": "https://heroes.thelazy.net/images/0/03/Advanced_Tactics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Monere",
            "name": "Monere",
            "img": "https://heroes.thelazy.net/images/f/fd/Hero_Monere_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Psychic_Elementals",
            "name": "Psychic Elementals",
            "img": "https://heroes.thelazy.net/images/0/0c/Specialty_Psychic_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Logistics",
            "name": "Basic Logistics",
            "img": "https://heroes.thelazy.net/images/7/75/Basic_Logistics.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Pasis",
            "name": "Pasis",
            "img": "https://heroes.thelazy.net/images/2/2d/Hero_Pasis_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Psychic_Elementals",
            "name": "Psychic Elementals",
            "img": "https://heroes.thelazy.net/images/0/0c/Specialty_Psychic_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Offense",
            "name": "Basic Offense",
            "img": "https://heroes.thelazy.net/images/c/c1/Basic_Offense.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Artillery",
            "name": "Basic Artillery",
            "img": "https://heroes.thelazy.net/images/4/4c/Basic_Artillery.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Thunar",
            "name": "Thunar",
            "img": "https://heroes.thelazy.net/images/2/22/Hero_Thunar.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Planeswalker",
            "name": "Planeswalker",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Earth_Elementals",
            "name": "Earth Elementals",
            "img": "https://heroes.thelazy.net/images/f/fa/Specialty_Earth_Elementals.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Tactics",
            "name": "Basic Tactics",
            "img": "https://heroes.thelazy.net/images/8/81/Basic_Tactics.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Estates",
            "name": "Basic Estates",
            "img": "https://heroes.thelazy.net/images/3/3a/Basic_Estates.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Aenain",
            "name": "Aenain",
            "img": "https://heroes.thelazy.net/images/2/20/Hero_Aenain_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Disrupting_Ray",
            "name": "Disrupting Ray",
            "img": "https://heroes.thelazy.net/images/8/85/Disrupting_Ray.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Air_Magic",
            "name": "Basic Air Magic",
            "img": "https://heroes.thelazy.net/images/4/44/Basic_Air_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Disrupting_Ray",
            "name": "Disrupting Ray",
            "img": "https://heroes.thelazy.net/images/8/85/Disrupting_Ray.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Brissa",
            "name": "Brissa",
            "img": "https://heroes.thelazy.net/images/c/c4/Hero_Brissa_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Air_Magic",
            "name": "Basic Air Magic",
            "img": "https://heroes.thelazy.net/images/4/44/Basic_Air_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Haste",
            "name": "Haste",
            "img": "https://heroes.thelazy.net/images/3/35/Haste.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Ciele",
            "name": "Ciele",
            "img": "https://heroes.thelazy.net/images/a/aa/Hero_Ciele_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Water_Magic",
            "name": "Basic Water Magic",
            "img": "https://heroes.thelazy.net/images/5/59/Basic_Water_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Magic_Arrow",
            "name": "Magic Arrow",
            "img": "https://heroes.thelazy.net/images/4/44/Magic_Arrow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Gelare",
            "name": "Gelare",
            "img": "https://heroes.thelazy.net/images/d/dd/Hero_Gelare_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Water_Magic",
            "name": "Basic Water Magic",
            "img": "https://heroes.thelazy.net/images/5/59/Basic_Water_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Dispel",
            "name": "Dispel",
            "img": "https://heroes.thelazy.net/images/7/7a/Dispel.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Grindan",
            "name": "Grindan",
            "img": "https://heroes.thelazy.net/images/8/8d/Hero_Grindan_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Gold",
            "name": "Gold",
            "img": "https://heroes.thelazy.net/images/a/a4/Specialty_Gold.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Earth_Magic",
            "name": "Basic Earth Magic",
            "img": "https://heroes.thelazy.net/images/4/48/Basic_Earth_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Slow",
            "name": "Slow",
            "img": "https://heroes.thelazy.net/images/f/f5/Slow.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Inteus",
            "name": "Inteus",
            "img": "https://heroes.thelazy.net/images/9/90/Hero_Inteus_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Fire_Magic",
            "name": "Basic Fire Magic",
            "img": "https://heroes.thelazy.net/images/0/00/Basic_Fire_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Bloodlust",
            "name": "Bloodlust",
            "img": "https://heroes.thelazy.net/images/1/11/Bloodlust.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Labetha",
            "name": "Labetha",
            "img": "https://heroes.thelazy.net/images/9/90/Hero_Labetha_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Earth_Magic",
            "name": "Basic Earth Magic",
            "img": "https://heroes.thelazy.net/images/4/48/Basic_Earth_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Stone_Skin",
            "name": "Stone Skin",
            "img": "https://heroes.thelazy.net/images/2/2d/Stone_Skin.png"
        }
    },
    {
        "hero": {
            "url": "https://heroes.thelazy.net/index.php/Luna",
            "name": "Luna",
            "img": "https://heroes.thelazy.net/images/3/34/Hero_Luna_%28HotA%29.png"
        },
        "class": {
            "url": "https://heroes.thelazy.net/index.php/Elementalist",
            "name": "Elementalist",
            "town": "Conflux"
        },
        "specialty": {
            "url": "https://heroes.thelazy.net/index.php/Fire_Wall",
            "name": "Fire Wall",
            "img": "https://heroes.thelazy.net/images/8/8b/Fire_Wall.png"
        },
        "skill1": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Wisdom",
            "name": "Basic Wisdom",
            "img": "https://heroes.thelazy.net/images/b/b1/Basic_Wisdom.png"
        },
        "skill2": {
            "url": "https://heroes.thelazy.net/index.php/Basic_Fire_Magic",
            "name": "Basic Fire Magic",
            "img": "https://heroes.thelazy.net/images/0/00/Basic_Fire_Magic.png"
        },
        "spell": {
            "url": "https://heroes.thelazy.net/index.php/Fire_Wall",
            "name": "Fire Wall",
            "img": "https://heroes.thelazy.net/images/8/8b/Fire_Wall.png"
        }
    }
];
