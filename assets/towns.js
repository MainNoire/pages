const towns = {
    "castle": {"name": "Castle", "url": "https://heroes.thelazy.net/index.php/Castle", "img": "https://heroes.thelazy.net/images/6/66/Town_portrait_Castle_small.gif"},
    "rampart": {"name": "Rampart", "url": "https://heroes.thelazy.net/index.php/Rampart", "img": "https://heroes.thelazy.net/images/d/dc/Town_portrait_Rampart_small.gif"},
    "tower": {"name": "Tower", "url": "https://heroes.thelazy.net/index.php/Tower", "img": "https://heroes.thelazy.net/images/3/32/Town_portrait_Tower_small.gif"},
    "inferno": {"name": "Inferno", "url": "https://heroes.thelazy.net/index.php/Inferno", "img": "https://heroes.thelazy.net/images/e/e5/Town_portrait_Inferno_small.gif"},
    "necropolis": {"name": "Necropolis", "url": "https://heroes.thelazy.net/index.php/Necropolis", "img": "https://heroes.thelazy.net/images/5/54/Town_portrait_Necropolis_small.gif"},
    "dungeon": {"name": "Dungeon", "url": "https://heroes.thelazy.net/index.php/Dungeon", "img": "https://heroes.thelazy.net/images/b/b0/Town_portrait_Dungeon_small.gif"},
    "stronghold": {"name": "Stronghold", "url": "https://heroes.thelazy.net/index.php/Stronghold", "img": "https://heroes.thelazy.net/images/a/a4/Town_portrait_Stronghold_small.gif"},
    "fortress": {"name": "Fortress", "url": "https://heroes.thelazy.net/index.php/Fortress", "img": "https://heroes.thelazy.net/images/f/f6/Town_portrait_Fortress_small.gif"},
    "conflux": {"name": "Conflux", "url": "https://heroes.thelazy.net/index.php/Conflux", "img": "https://heroes.thelazy.net/images/4/48/Town_portrait_Conflux_small.gif"},
    "cove": {"name": "Cove", "url": "https://heroes.thelazy.net/index.php/Cove", "img": "https://heroes.thelazy.net/images/4/4a/Town_portrait_Cove_small.gif"}
}
