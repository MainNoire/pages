var banned_by_default_heroes = [
    "Catherine", "Roland", "Sir Mullich",  // Castle
    "Gelu", "Thorgrim", // Rampart
    "Dracon",  // Tower
    "Xeron",  // Inferno
    "Haart Lich",  // Necro
    "Mutare Drake",  // Dungeon
    "Boragus", "Kilgor",  // Stronghold
    "Adrienne",  // Fortress
    "Bidley", "Tark",  // Cove
];